@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <painel titulo="Cargos Cadastrados">
                <navegacao v-bind:lista="{{$listaPaginas}}"></navegacao>
                <tabela-lista 
                    v-bind:titulos="['Id','Cargo', 'Ações']"
                    v-bind:itens="{{$listaCargos}}"
                    criar="#criar"
                    detalhe="/admin/cargos/"
                    editar="/admin/cargos/"
                    excluir="/admin/cargos/"
                    token="{{ csrf_token() }}"
                    modal="Sim"
                ></tabela-lista>
            </painel>
            

            <modal nome="criar">
                <painel titulo="Cadastrar Cargo">
                    <formulario css="" action="{{route('cargos.store')}}" method="post" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="txtNome">Cargo</label>
                            <input type="text" class="form-control" id="cargo" name="cargo">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Salvar</button>
                    </formulario>
                </painel>
            </modal>

            <modal nome="editar">
                <painel titulo="Editar Cargo">
                    <formulario css="" v-bind:action="'/admin/cargos/' + $store.state.item.id" method="put" enctype="" token="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="txtNome">Cargo</label>
                                <input type="text" class="form-control" id="cargo" name="cargo" v-model="$store.state.item.cargo">
                            </div>
                            <div class="form-group">
                                <label for="txtNmStatus">Status</label>
                                <select name="status" id="status" class="form-control" v-model="$store.state.item.status">
                                    <option value="">Selecione</option>
                                    <option value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        <button class="btn btn-primary">Atualizar</button>
                    </formulario>
                </painel>
            </modal>

        </div>
    </div>
</div>
@endsection