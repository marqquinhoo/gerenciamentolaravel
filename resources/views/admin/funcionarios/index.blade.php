@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <painel titulo="Funcionarios Cadastrados">
                <navegacao v-bind:lista="{{$listaPaginas}}"></navegacao>
                <tabela-lista 
                    v-bind:titulos="['Id','Nome','Email','Setor', 'Cargo', 'Ações']"
                    v-bind:itens="{{$listaFuncionarios}}"
                    v-bind:setores="{{$listaSetores}}"
                    criar="#criar"
                    detalhe="/admin/funcionarios/"
                    editar="/admin/funcionarios/"
                    excluir="/admin/funcionarios/"
                    token="{{ csrf_token() }}"
                    modal="Sim"
                ></tabela-lista>
            </painel>
            

            <modal nome="criar">
                <painel titulo="Cadastrar Funcionario">
                    <formulario css="" action="{{route('funcionarios.store')}}" method="post" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="txtNome">Nome</label>
                            <input type="text" class="form-control" id="nome" name="nome">
                        </div>
                        <div class="form-group">
                            <label for="txtNome">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Setor</label>
                            <select name="idSetor" id="idSetor" class="form-control">
                                <option value="">Selecione</option>
                                <option value="1">TI</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Cargo</label>
                            <select name="idCargo" id="idCargo" class="form-control">
                                <option value="">Selecione</option>
                                <option value="1">Administrador de Redes</option>
                                <option value="2">Service Desk</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Salvar</button>
                    </formulario>
                </painel>
            </modal>

            <modal nome="editar">
                <painel titulo="Editar Funcionario">
                    <formulario css="" v-bind:action="'/admin/funcionarios/' + $store.state.item.id" method="put" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>
                            <input type="text" class="form-control" id="nmSetor" name="nmSetor" v-model="$store.state.item.nome">
                        </div>
                        <div class="form-group">
                            <label for="txtNome">Email</label>
                            <input type="email" class="form-control" id="email" name="email" v-model="$store.state.item.email">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Setor</label>
                            <select name="idSetor" id="idSetor" class="form-control"  v-model="$store.state.item.idSetor"> 
                                <option value="">Selecione</option>
                                <option value="1">TI</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Cargo</label>
                            <select name="idCargo" id="idCargo" class="form-control"  v-model="$store.state.item.idCargo">
                                <option value="">Selecione</option>
                                <option value="1">Administrador de Redes</option>
                                <option value="2">Service Desk</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Atualizar</button>
                    </formulario>
                </painel>
            </modal>

            <modal nome="excluir">
                <painel titulo="Excluir Setor">
                    <formulario css="" v-bind:action="'/admin/setores/' + $store.state.item.id" method="put" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome Setor</label>
                            <input type="text" class="form-control" id="nmSetor" name="nmSetor" v-model="$store.state.item.nome">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Atualizar</button>
                    </formulario>
                </painel>
            </modal>

        </div>
    </div>
</div>
@endsection