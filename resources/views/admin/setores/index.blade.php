@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <painel titulo="Setores Cadastrados">
                <navegacao v-bind:lista="{{$listaPaginas}}"></navegacao>
                <tabela-lista 
                    v-bind:titulos="['Id','Titulo','Status','Ações']"
                    v-bind:itens="{{$listaSetores}}"
                    criar="#criar"
                    detalhe="/admin/setores/"
                    editar="/admin/setores/"
                    excluir="/admin/setores/"
                    token="{{ csrf_token() }}"
                    modal="Sim"
                ></tabela-lista>
            </painel>

            <modal nome="criar">
                <painel titulo="Cadastrar Setor">
                    <formulario css="" action="{{route('setores.store')}}" method="post" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="txtNmSetor">Nome Setor</label>
                            <input type="text" class="form-control" id="nome" name="nome">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Salvar</button>
                    </formulario>
                </painel>
            </modal>

            <modal nome="editar">
                <painel titulo="Editar Setor">
                    <formulario css="" v-bind:action="'/admin/setores/' + $store.state.item.id" method="put" enctype="" token="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome Setor</label>
                            <input type="text" class="form-control" id="nmSetor" name="nmSetor" v-model="$store.state.item.nome">
                        </div>
                        <div class="form-group">
                            <label for="txtNmStatus">Status</label>
                            <select name="status" id="status" class="form-control" v-model="$store.state.item.status">
                                <option value="">Selecione</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <button class="btn btn-primary">Atualizar</button>
                    </formulario>
                </painel>
            </modal>


        </div>
    </div>
</div>
@endsection