@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <painel titulo="Dashboard">                   
                    <navegacao v-bind:lista="{{$listaPaginas}}"></navegacao>
                    <div class="row">
                        <div class="col-md-4">
                            <caixa qte="1" titulo="Funcionarios Cadastrados" url="admin/funcionarios" cor="#f5645d" icone="ion ion-person-add" >
                                <p></p>
                                <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                            </caixa>
                        </div>
                        
                        <div class="col-md-4">
                        <caixa qte="1" titulo="Setores Cadastrados" url="admin/setores" cor="#9e9ed4" icone="ion ion-network" >
                                <p></p>
                                <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                            </caixa>
                        </div>
                        
                        <div class="col-md-4">
                        <caixa qte="1" titulo="Cargos Cadastrados" url="admin/cargos" cor="#9aefc0" icone="ion ion-ribbon-a" >
                            <p></p>
                            <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                        </caixa>
                    </div>

                </painel>
            </div>
        </div>
    </div>
@endsection